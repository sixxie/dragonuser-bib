# Dragon User BibTex file

For all those citations you make of articles in Dragon User in your thesis.
More seriously: the abstracts are really the "added value" here.  The Dragon
User PDFs available from [The Dragon Archive] are well-indexed.

[The Dragon Archive](https://archive.worldofdragon.org/)

Sometimes mistakes are from the text (e.g, "chandalier"), but you should
generally assume any you find are mine (lots of evidence in the git history to
back that up), so please let me know of any speling errorrs you spot.  Or
indeed, if an abstract doesn't seem terribly descriptive.

A tiny LaTeX document (and Makefile) is now included to make use of the
bibliography and generate a [PDF] simply listing everything in order.

[PDF](http://www.6809.org.uk/dragon/dragonuser-bib.pdf)
