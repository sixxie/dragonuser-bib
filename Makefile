# Dragon User BibTeX

.PHONY: all
all: dragonuser-bib.pdf

###

CLEAN =

###

CLEAN += dragonuser-bib.pdf

dragonuser-bib.pdf: dragonuser.bib

dragonuser-bib.pdf: dragonuser-bib.tex
	xelatex -halt-on-error -draftmode $<
	biber dragonuser-bib
	xelatex -halt-on-error -draftmode $<
	xelatex -halt-on-error $<

CLEAN += dragonuser-bib.aux dragonuser-bib.bbl dragonuser-bib.bcf
CLEAN += dragonuser-bib.blg dragonuser-bib.log dragonuser-bib.run.xml

###

.PHONY: clean
clean:
	rm -f $(CLEAN)
